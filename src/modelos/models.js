const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;

var userSchema = Schema({
    name: String,
    email: String,
    password: String,
    phone: String,
    photo: String,
    rol: String,
    favorites: [String],
    block: Boolean,
    idParking: String,
    reports:[],
});

var parkingSchema = Schema({
    status: String,
    tariff: Number,
    tariffType: String,
    location: String,
    numberAvailable: Number,
    numberCapacity: Number,
    services: [String],
    photos: [String],
    dpi: String,
    criminalRecords: String,
    acceptTerm: Boolean,
    authorized: Boolean,
    rating: [],
    reports: [],
});

//noticias de parqueo
var noticeSchema = Schema({
    photos: [String],
    text: String,
    user: String,
    parkingLot:String,
    votes:[]
});

//Mongoose.deleteModel('Parking');
var User = Mongoose.model('User', userSchema);
var Parking = Mongoose.model('Parking', parkingSchema);
var Notice = Mongoose.model('Notice', noticeSchema);

module.exports.User = User;
module.exports.Parking = Parking;
module.exports.Notice = Notice;
