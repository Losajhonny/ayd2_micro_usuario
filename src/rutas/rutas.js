const Express = require('express');
const Mongoose = require('mongoose');
const Router = Express.Router();

const url = "mongodb+srv://admin:admin@dbparqueo.kwbsx.mongodb.net/dbparqueo";
Mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false }, (err) => {
        if(err)
            console.log('error conecting with mongodb');
        console.log('connected...');
    });

// metodos get
const getAllUsers = require('../metodos/get/get-all-users');
const getAllParking = require('../metodos/get/get-all-parking');
const getListNoticia = require('../metodos/get/get-listNoticia');
const getListNoticieFeed = require('../metodos/get/get-listNoticeFeed');


// metodos post
const postRegister = require('../metodos/post/post-register');
const postLogin = require('../metodos/post/post-login');
const postUpdateRol = require('../metodos/post/post-update-rol');
const postUpdateAuthorized = require('../metodos/post/post-update-authorized');
const postParkingId = require('../metodos/post/post-parking-id');
const postUpdatePhoto = require('../metodos/post/post-update-photo');
const postUpdateDpi = require('../metodos/post/post-update-dpi');
const postUpdateCriminalRecords = require('../metodos/post/post-update-criminalRecords');
const postNoticiaParking = require('../metodos/post/post-addParkingNoticia');
const postUpdateAcceptTerm = require('../metodos/post/post-update-acceptTerm');
const postReportRegular = require('../metodos/post/post-reportRegular');
const postReportParking = require('../metodos/post/post-reportParking');
const postVoteCounting = require('../metodos/post/post-voteCounting');
const postVoteNoticie = require('../metodos/post/post-voteNoticie');

// metodos post
const updateFavoriteParking = require('../metodos/update/update-addFavoriteParking');

// router get
Router.get('/', (req, res) => { return res.status(200).send("servidor"); });
Router.get('/all-users', getAllUsers);
Router.get('/all-parking', getAllParking);
Router.get('/get-listNotice', getListNoticia);
Router.get('/get-listNoticeFeed',getListNoticieFeed);


// router post
Router.post('/register', postRegister);
Router.post('/login', postLogin);
Router.post('/update-rol', postUpdateRol);
Router.post('/update-authorized', postUpdateAuthorized);
Router.post('/get-parking', postParkingId);
Router.post('/update-photo', postUpdatePhoto);
Router.post('/update-dpi', postUpdateDpi);
Router.post('/update-criminalRecords', postUpdateCriminalRecords);
Router.post('/post-addParkingNoticia', postNoticiaParking);
Router.post('/update-acceptTerm', postUpdateAcceptTerm);
Router.post('/post-reportRegular',postReportRegular);
Router.post('/post-reportParking',postReportParking);
Router.post('/post-voteCounting',postVoteCounting);
Router.post('/post-voteNoticia',postVoteNoticie);

// router post
Router.put('/update-addFavoriteParking', updateFavoriteParking);

module.exports = Router;

/*
servidor 1
- rregistro
- login
- update perfiles

servidor 2
- noticias
- promociones
- bloqueos parqueos y usuarios
*/
