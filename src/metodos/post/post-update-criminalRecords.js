const Parking = require('../../modelos/models').Parking;

function postUpdateCriminalRecords (req, res) {
    const { id, url } = req.body;

    Parking.findByIdAndUpdate(id, {criminalRecords: url}, (err, park) => {
        if(err)     return res.status(500).send('Error interno del servidor. (Parking.findByIdAndUpdate)');
        if(!park)    return res.status(400).send("Su cuenta no esta registrado");
        // este park no esta actualizado
        return res.status(200).send({});
    });
}

module.exports = postUpdateCriminalRecords;