const user_reported = require('../../modelos/models').User;

async function reportRegular(req,res){
    //console.log(req.body);
    var report = {idRegular:req.body.idRegular}
    result = await user_reported.findById(req.body.idReported).elemMatch("reports", report);
    //console.log(result);
    if(!result){
        result = await user_reported.findByIdAndUpdate(
            req.body.idReported,
            { $addToSet: { reports: report } });
            
        res.send({success: true});
    }else{
        res.send({success: false});
    }
    console.log(result);
}

module.exports = reportRegular


