const User = require('../../modelos/models').User;

function postUpdatePhoto (req, res) {
    const { id, url } = req.body;

    User.findByIdAndUpdate(id, {photo: url}, (err, usr) => {
        if(err)     return res.status(500).send('Error interno del servidor. (user.findByIdAndUpdate)');
        if(!usr)    return res.status(400).send("Su cuenta no esta registrado");
        // este usr no esta actualizado
        return res.status(200).send({});
    });
}

module.exports = postUpdatePhoto;