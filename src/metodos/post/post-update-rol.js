const User = require('../../modelos/models').User;
const Parking = require('../../modelos/models').Parking;

async function postUpdateRol (req, res) {
    const { id, rol } = req.body;
    
    var params = { rol: rol };
    await User.findByIdAndUpdate(id, params, (err, usr) => {
        if(err)     return res.status(500).send('Error interno del servidor. (user.findByIdAndUpdate)');
        if(!usr)    return res.status(400).send("Su cuenta no esta registrado");
        // este usr no esta actualizado
        return usr;
    });

    if (rol === 'owner') {
        params = { idParking: null };

        var nuevoParking = new Parking({
            status: null,
            tariff: 0.0,
            tariffType: null,
            location: null,
            numberAvailable: 0,
            numberCapacity: 0,
            services: [],
            photos: [],
            dpi: null,
            criminalRecords: null,
            acceptTerm: false,
            authorized: false,
            rating: [],
            reports: []
        });
        
        var parking = await nuevoParking.save();
        console.log('parking: ' + parking._id);
        params.idParking = parking._id;

        await User.findByIdAndUpdate(id, params, (err, usr) => {
            if(err) return res.status(500).send('Error interno del servidor. (user.findByIdAndUpdate)');
            if(!usr) return res.status(400).send("Su cuenta no esta registrado");
            // este usr no esta actualizado
            return usr;
        });
    }

    User.findById(id, (err, usr) => {
        if(err)     return res.status(500).send('Error interno del servidor. (user.findById)');
        if(!usr)    return res.status(400).send("Su cuenta no esta registrado");

        var user = {
            id: usr._id,
            name: usr.name,
            email: usr.email,
            phone: usr.phone,
            photo: usr.photo,
            rol: usr.rol,
            favorites: usr.favorites,
            block: usr.block,
            idParking: usr.idParking,
        };

        return res.status(200).send(user);
    });
}

module.exports = postUpdateRol;
