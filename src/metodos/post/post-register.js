const User = require('../../modelos/models').User;

function postRegister (req, res) {
    const {
        name,
        email,
        password,
        phone,
        photo
    } = req.body;

    let datos = {
        name: name,
        email: email,
        password: password,
        phone: phone,
        photo: photo,
        rol: null,
        favorites: [],
        block: false,
        idParking: null,
        reports:[]
    };

    User.findOne({ email: email }, (err, usr) => {
        if(err)
            return res.status(500).send('Error interno del servidor. (user.findOne)');

        if(usr)
            return res.status(400).send("Su cuenta ya esta registrado");

        let nusr = new User(datos);
        nusr.save(() => {
            console.log('user: ' + email);
        });
        
        var user = {
            id: nusr._id,
            name: nusr.name,
            email: nusr.email,
            phone: nusr.phone,
            photo: nusr.photo,
            rol: nusr.rol,
            favorites: nusr.favorites,
            block: nusr.block,
            idParking: nusr.idParking,
            reports:nusr.reports
        };

        return res.status(200).send(user);
    });
}

module.exports = postRegister;
