const { User, Parking } = require('../../modelos/models');

function postParkingId(req, res) {
    const { id, idParking } = req.body;

    User.findById(id, (err, user) => {
        if(err)
                return res.status(500).send({});
            
        if(!user)
            return res.status(400).send('Su usuario no existe');

        Parking.findById(idParking, (err, park) => {
            if(err)
                return res.status(500).send({});
            
            if(!park)
                return res.status(400).send('Su parqueo no existe');
    
            var parking = {
                id: user._id,
                idParking: park._id,
                status: park.status,
                tariff: park.tariff,
                tariffType: park.tariffType,
                location: park.location,
                numberAvailable: park.numberAvailable,
                numberCapacity: park.numberCapacity,
                services: park.services,
                photos: park.photos,
                dpi: park.dpi,
                criminalRecords: park.criminalRecords,
                acceptTerm: park.acceptTerm,
                authorized: park.authorized,
                rating: park.rating,
                reports: park.reports,
                name: user.name,
                email: user.email,
                phone: user.phone,
                photo: user.photo,
                rol: user.rol,
                favorites: user.favorites,
                block: user.block
            };
    
            return res.status(200).send(parking);
        });
    });
}

module.exports = postParkingId;