const NOTICE_CONFIG = require('../../modelos/models').Notice; //la tabla para guardar las noticias

/**
 * Funcion para guardar noticias de un parqueo
 * 
 * cuando se envie una peticion
 * se debe de estar seguro si el usuario y parque existe
 * 
 * en este caso se trabaja en un usuario de sesion y un parqueo existente
 * @param {*} req
 * @param {*} res
 */

 function noticia_request(req,res){
   //console.log(req.body);

   //obtengo los valores de la noticia
   var noticia_params = {
        photos: req.body.photos,
        text: req.body.text,
        user: req.body.user,
        parkingLot: req.body.parkingLot
   };
   
   //ahora lo ingreso a la bd, 
   var newNoticia = new NOTICE_CONFIG(noticia_params);

   newNoticia.save(function(){
      console.log(newNoticia);
       console.log('noticia almacenada');
   });
   
   if(req.err) return handleError(req.err);
   else return res.send({bandera: true});

 }
 module.exports = noticia_request;
