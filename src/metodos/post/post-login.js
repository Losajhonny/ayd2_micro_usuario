const User = require('../../modelos/models').User;

function postLogin(req, res) {
    const { email, password, provider } = req.body;
    
    var conditions = null;
    if(provider === 'email') {
        conditions = { email: email, password: password };
    } else {
        conditions = { email: email };
    }

    User.findOne(conditions, (err, usr) => {
        if (err)
            return res.status(500).send('Error interno del servidor. (user.findOne)');
            
        if (!usr)
            return res.status(400).send("Su cuenta no esta registrado");
        
        var user = {
            id: usr._id,
            name: usr.name,
            email: usr.email,
            phone: usr.phone,
            photo: usr.photo,
            rol: usr.rol,
            favorites: usr.favorites,
            block: usr.block,
            idParking: usr.idParking,
        }

        return res.status(200).send(user);
    });
}

module.exports = postLogin;
