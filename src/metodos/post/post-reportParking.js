const USER_PARKING = require('../../modelos/models').User;

async function reportParking(req,res){
    console.log(req.body);
    var report = {idRegular:req.body.idRegular}
    result = await USER_PARKING.findById(req.body.idParking).elemMatch("reports", report);
    console.log(result);
    if(!result){
        result = await USER_PARKING.findByIdAndUpdate(
            req.body.idParking,
            { $addToSet: { reports: report } });
        res.send({success: true});
    }else{
        res.send({success: false});
    }
    console.log(result);
}

module.exports = reportParking

