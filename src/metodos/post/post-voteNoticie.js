const PARKING_NOTICIA = require('../../modelos/models').Notice;
const PARKING_USER = require('../../modelos/models').Parking;
const REGULAR_USER = require('../../modelos/models').User;

function postVoteParking(req, res) {
    console.log(req.body);
    REGULAR_USER.findById(req.body.idUserRegular,(err, regular) => {
        if(err){
            console.log("error regular");
            return res.send({error:"idRegular"});
        }else{     
            var Paramvote = {
                vote: req.body.flag,
                idUser: req.body.idUserRegular
             }/** */
             PARKING_NOTICIA.findByIdAndUpdate(
                req.body.idParkingNoticia,
                { $pull: { votes: {idUser: req.body.idUserRegular} } },
                (err, user) => {
                     if(err) return handleError(err);
                     //console.log(user);
                    });
                PARKING_NOTICIA.findByIdAndUpdate(
                    req.body.idParkingNoticia,
                    { $push: { votes: Paramvote } },
                    (err, user) => {
                        if(err) return handleError(err);
                        //console.log(user);
                    });
                    return res.send({result:"success"});
                }
    });
}

module.exports = postVoteParking;