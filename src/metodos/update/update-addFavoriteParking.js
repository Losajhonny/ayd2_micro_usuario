const USER_REGULAR = require('../../modelos/models').User;
/**
 * Funcion para guardar en la tabla de usuario regular 
 * los parqueos en favoritos
 *
 */

async function add_favoriteParking(req, res) {

    let user = await USER_REGULAR.findById(req.body.id_user).exec();
    let fav = user.favorites;

    let cont = 0;
    for(let i = 0; i < fav.length; i++) {
        if(fav[i] === req.body.id_parking) {
            cont++;
        }
    }
    if(cont === 0) {
        fav.push(req.body.id_parking);
        await USER_REGULAR.findByIdAndUpdate(req.body.id_user, {favorites: fav}).exec();
        console.log(req.body.id_parking+" agregado a favorito");
    }
    else {
        console.log(req.body.id_parking+" existe en favorito");
    }
    
    var results = await USER_REGULAR.findById(req.body.id_user).exec();
    //console.log(results);
    return res.send(results);
}
module.exports = add_favoriteParking;
