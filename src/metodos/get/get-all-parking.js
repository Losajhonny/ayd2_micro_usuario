const User = require('../../modelos/models').User;
const Parking = require('../../modelos/models').Parking;

function getAllParking(req, res) {
    User.find(async (err, users) => {
        if(err)
            return res.status(500).send({});

        var parkings = [];

        for (let i = 0; i < users.length; i++) {
            const user = users[i];
            
            if(user.rol === 'owner') {
                const park = await Parking.findById(user.idParking, (err, parking) => {
                    if(err)
                        return res.status(500).send({});
                    return parking;
                });
    
                parkings.push({
                    id: user._id,
                    idParking: park._id,
                    name: user.name,
                    email: user.email,
                    phone: user.phone,
                    photo: user.photo,
                    rol: user.rol,
                    favorites: user.favorites,
                    block: user.block,
                    status: park.status,
                    tariff: park.tariff,
                    tariffType: park.tariffType,
                    location: park.location,
                    numberAvailable: park.numberAvailable,
                    numberCapacity: park.numberCapacity,
                    services: park.services,
                    photos: park.photos,
                    dpi: park.dpi,
                    criminalRecords: park.criminalRecords,
                    acceptTerm: park.acceptTerm,
                    authorized: park.authorized,
                    rating: park.rating,
                    reports: park.reports
                });
            }
        }
        return res.status(200).send(parkings);
    });
}

module.exports = getAllParking;