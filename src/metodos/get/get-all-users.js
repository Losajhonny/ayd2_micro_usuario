const User = require('../../modelos/models').User;

function getAllUsers(req, res) {
    User.find((err, users) => {
        if(err)
            return res.status(500).send({});
        return res.status(200).send(users);
    });
}

module.exports = getAllUsers;