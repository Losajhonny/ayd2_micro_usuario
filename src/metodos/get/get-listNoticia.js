/*
* Modulo para listar noticas en forma general
*/

const noticiaConfig = require('../../modelos/models').Notice;

function getListNoticia(req, res) {
    noticiaConfig.find((err, noticias) => {
        if(err) return handleError(err);

        return res.send(noticias);
    })
}

module.exports = getListNoticia;

