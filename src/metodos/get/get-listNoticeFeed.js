
const NOTICE_PARKING = require('../../modelos/models').Notice;
const USER_REGULAR = require('../../modelos/models').User;
const USER_PARKING = require('../../modelos/models').Parking;

const waitFor = (ms) => new Promise(r => setTimeout(r, ms));

async function getListNoticesFeed(req, res) {
    var notice_collection = [];
    let users = await NOTICE_PARKING.find({}).exec();
    await asyncForEach(users,async (element)=>{
        //await waitFor(100);
        //console.log(element._id + ' ' + element.user + ' ' + element.parkingLot);
        let RegularUser = await USER_REGULAR.findById(element.user);
        let ParkingUser = await USER_PARKING.findById(element.parkingLot);
        let data = {
            "infoUser": RegularUser,
            "infoParking": ParkingUser,
            "notice": element
        }
        //console.log(element);
        await waitFor(100);
        notice_collection.push(data);
        
    });
    //users.forEach();
    //console.log('elementos enviados: ' + notice_collection.length);
    res.send(notice_collection);
}
async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
}

module.exports = getListNoticesFeed

