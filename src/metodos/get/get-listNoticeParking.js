/**
 * Modulo para tener una lista de noticas por parqueo
 */

const NOTICE = require('../../modelos/models').parkingNoticia;

async function getListNoticeParking(req, res) {
    //console.log(req.body);

    let notices = await NOTICE.find().exec();
    let noticeParking = [];

    for(let i = 0; i < notices.length; i++) {
        let notice = notices[i];

        if(notice.parkingLot === req.body.idParking) {
            noticeParking.push(notice);
        }
    }

    return res.send(noticeParking);

    /*let notices = await NOTICE.find((err, noticesSearch) => {
        let noticeParking = [];
        for(let i = 0; i < noticesSearch.length; i++) {
            let notice = noticesSearch[i];
            if(notice.parkingLot === req.body.idParking) {
                noticeParking.push(notice);
            }
        }
        return res.send(noticeParking);
    });*/
}

module.exports = getListNoticeParking;
