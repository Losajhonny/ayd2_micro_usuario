const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const routes = require('./rutas/rutas');

// settings
app.set('port', 3000);
app.use(cors());
app.use(bodyParser.urlencoded({/*limit: "50mb", */extended: false}));
app.use(bodyParser.json({/*limit: '50mb', */extended: false}));
app.use(routes);

// iniciar servidor
app.listen(app.get('port'), () => { console.log('server on port', app.get('port')); });

module.exports = app;