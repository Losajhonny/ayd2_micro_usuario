#base image
FROM node:12-alpine

WORKDIR /app
ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

# install and cache app dependencies
COPY package.json /app/
RUN npm install

COPY . /app

CMD [ "npm", "start" ]

EXPOSE 3000

